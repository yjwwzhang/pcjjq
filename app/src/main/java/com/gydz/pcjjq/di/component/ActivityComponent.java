package com.gydz.pcjjq.di.component;

import com.gydz.pcjjq.ui.MainActivity;

import dagger.Component;

/**
 * 创建时间:2018/6/22 10:08
 */
@Component
public interface ActivityComponent {

    void inject(MainActivity mainActivity);
}
