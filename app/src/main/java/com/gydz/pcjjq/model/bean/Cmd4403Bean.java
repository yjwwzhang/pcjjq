package com.gydz.pcjjq.model.bean;

import com.gydz.pcjjq.model.bean.PassengerInfoBean;

import java.util.ArrayList;
import java.util.List;

/**
 * 创建时间:2018/6/25 18:05
 */
public class Cmd4403Bean {

    private long currentTime = 0;
    private int price = 330;
    private int discount = 100;
    private int carSeatState = 0;
    private int carSpeed = 0;
    private int carDrivingState = 0;
    private int passengerTotal = 0;
    private List<PassengerInfoBean> passengerInfos = new ArrayList<PassengerInfoBean>() {
        {
            add(new PassengerInfoBean(1));
            add(new PassengerInfoBean(2));
            add(new PassengerInfoBean(3));
            add(new PassengerInfoBean(4));
        }
    };

    @Override
    public String toString() {
        return "Cmd4403Bean{" +
                "currentTime=" + currentTime +
                ", price=" + price +
                ", discount=" + discount +
                ", carSeatState=" + carSeatState +
                ", carSpeed=" + carSpeed +
                ", carDrivingState=" + carDrivingState +
                ", passengerTotal=" + passengerTotal +
                ", passengerInfos=" + passengerInfos +
                '}';
    }

    public Cmd4403Bean() {
    }

    public Cmd4403Bean(long currentTime, int price, int discount, int carSeatState, int carSpeed, int carDrivingState, int passengerTotal, List<PassengerInfoBean> passengerInfos) {
        this.currentTime = currentTime;
        this.price = price;
        this.discount = discount;
        this.carSeatState = carSeatState;
        this.carSpeed = carSpeed;
        this.carDrivingState = carDrivingState;
        this.passengerTotal = passengerTotal;
        this.passengerInfos = passengerInfos;
    }

    public int getPassengerTotal() {
        return passengerTotal;
    }

    public void setPassengerTotal(int passengerTotal) {
        this.passengerTotal = passengerTotal;
    }

    public long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(long currentTime) {
        this.currentTime = currentTime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public int getCarSeatState() {
        return carSeatState;
    }

    public void setCarSeatState(int carSeatState) {
        this.carSeatState = carSeatState;
    }

    public int getCarSpeed() {
        return carSpeed;
    }

    public void setCarSpeed(int carSpeed) {
        this.carSpeed = carSpeed;
    }

    public int getCarDrivingState() {
        return carDrivingState;
    }

    public void setCarDrivingState(int carDrivingState) {
        this.carDrivingState = carDrivingState;
    }

    public List<PassengerInfoBean> getPassengerInfos() {
        return passengerInfos;
    }

    public void setPassengerInfos(List<PassengerInfoBean> passengerInfos) {
        this.passengerInfos = passengerInfos;
    }
}
