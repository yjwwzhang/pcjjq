package com.gydz.pcjjq.model.bean;

import com.chad.library.adapter.base.entity.MultiItemEntity;

/**
 * 创建时间:2018/6/25 17:53
 */
public class PassengerInfoBean implements MultiItemEntity{

    private int passengerNumber;
    private long mileage = 0;
    private long waitTime = 0;
    private long shouldPay = 0;
    private long realPay = 0;
    private int seatStatus = 0;
    private int pauseStatus = 0;
    private String lastCmdContent = "";

    @Override
    public String toString() {
        return "PassengerInfoBean{" +
                "passengerNumber=" + passengerNumber +
                ", mileage=" + mileage +
                ", waitTime=" + waitTime +
                ", shouldPay=" + shouldPay +
                ", realPay=" + realPay +
                ", seatStatus=" + seatStatus +
                ", pauseStatus=" + pauseStatus +
                ", lastCmdContent='" + lastCmdContent + '\'' +
                '}';
    }

    public PassengerInfoBean() {
    }

    public PassengerInfoBean(int passengerNumber) {
        this.passengerNumber = passengerNumber;
    }

    public PassengerInfoBean(int passengerNumber, long mileage, long waitTime, long shouldPay, long realPay, int seatStatus, int pauseStatus, String lastCmdContent) {
        this.passengerNumber = passengerNumber;
        this.mileage = mileage;
        this.waitTime = waitTime;
        this.shouldPay = shouldPay;
        this.realPay = realPay;
        this.seatStatus = seatStatus;
        this.pauseStatus = pauseStatus;
        this.lastCmdContent = lastCmdContent;
    }

    public String getLastCmdContent() {
        return lastCmdContent;
    }

    public void setLastCmdContent(String lastCmdContent) {
        this.lastCmdContent = lastCmdContent;
    }

    public int getPassengerNumber() {
        return passengerNumber;
    }

    public void setPassengerNumber(int passengerNumber) {
        this.passengerNumber = passengerNumber;
    }

    public long getMileage() {
        return mileage;
    }

    public void setMileage(long mileage) {
        this.mileage = mileage;
    }

    public long getWaitTime() {
        return waitTime;
    }

    public void setWaitTime(long waitTime) {
        this.waitTime = waitTime;
    }

    public long getShouldPay() {
        return shouldPay;
    }

    public void setShouldPay(long shouldPay) {
        this.shouldPay = shouldPay;
    }

    public long getRealPay() {
        return realPay;
    }

    public void setRealPay(long realPay) {
        this.realPay = realPay;
    }

    public int getSeatStatus() {
        return seatStatus;
    }

    public void setSeatStatus(int seatStatus) {
        this.seatStatus = seatStatus;
    }

    public int getPauseStatus() {
        return pauseStatus;
    }

    public void setPauseStatus(int pauseStatus) {
        this.pauseStatus = pauseStatus;
    }

    @Override
    public int getItemType() {
        return seatStatus;
    }


}
