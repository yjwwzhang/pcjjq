package com.gydz.pcjjq.base;

/**
 * 创建时间:2018/6/22 09:46
 */
public interface BasePresenter<T extends BaseView> {
    void attachView(T view);
    void detachView();
}
