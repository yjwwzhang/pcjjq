package com.gydz.pcjjq.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ScreenUtils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.yokeyword.fragmentation.SupportActivity;

/**
 * 创建时间:2018/6/22 09:22
 */
public abstract class SimpleActivity extends SupportActivity {

    private Unbinder mBind;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ScreenUtils.getScreenHeight() > ScreenUtils.getScreenWidth()){
            ScreenUtils.setLandscape(this);
        }
        Window window = getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION|View.SYSTEM_UI_FLAG_IMMERSIVE;
        window.setAttributes(params);
        setContentView(getLayoutId());
        mBind = ButterKnife.bind(this);
        onViewCreated();
        EventBus.getDefault().register(this);
        initEventAndData();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBind.unbind();
        EventBus.getDefault().unregister(this);
    }

    protected  void onViewCreated() {

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEventNothing(String s) {
        LogUtils.e("不要用String传递信息，请自定义event数据类型");
    }

    protected abstract int getLayoutId();

    protected abstract void initEventAndData();
}
