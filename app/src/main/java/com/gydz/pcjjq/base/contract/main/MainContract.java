package com.gydz.pcjjq.base.contract.main;

import com.gydz.pcjjq.base.BasePresenter;
import com.gydz.pcjjq.base.BaseView;

/**
 * 创建时间:2018/6/22 10:13
 */
public interface MainContract {

    interface View extends BaseView {

    }

    interface Presenter extends BasePresenter<View> {
        void test();
    }
}
