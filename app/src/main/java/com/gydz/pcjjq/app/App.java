package com.gydz.pcjjq.app;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.multidex.MultiDexApplication;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ServiceUtils;
import com.blankj.utilcode.util.Utils;
import com.gydz.pcjjq.event.CmdT4481Event;
import com.gydz.pcjjq.model.service.SerialControlService;

import org.greenrobot.eventbus.EventBus;

/**
 * 创建时间:2018/6/21 14:27
 */
public class App extends MultiDexApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        Utils.init(this);
        ServiceUtils.bindService(SerialControlService.class, new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
                LogUtils.e("服务已连接");
                EventBus.getDefault().post(new CmdT4481Event());
            }

            @Override
            public void onServiceDisconnected(ComponentName componentName) {
                LogUtils.e("服务断开连接");
            }
        }, BIND_AUTO_CREATE);

    }
}
