package com.gydz.pcjjq.app;

/**
 * 创建时间:2018/6/22 09:39
 */
public interface Constants {

    int ITEM_PASSENGER_INFO_1 = 1;
    int ITEM_PASSENGER_INFO_2 = 0;

    String PASSENGER_INFO_JSON = "passenger_info_json";

    String IS_GPS = "is_gps";

}
