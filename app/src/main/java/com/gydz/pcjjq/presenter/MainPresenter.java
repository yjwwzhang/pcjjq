package com.gydz.pcjjq.presenter;

import com.blankj.utilcode.util.LogUtils;
import com.gydz.pcjjq.base.RxPresenter;
import com.gydz.pcjjq.base.contract.main.MainContract;

import javax.inject.Inject;

/**
 * 创建时间:2018/6/22 10:15
 */
public class MainPresenter extends RxPresenter<MainContract.View> implements MainContract.Presenter {

    @Inject
    public MainPresenter() {
    }

    @Override
    public void test() {
        LogUtils.e("Test");
    }
}
