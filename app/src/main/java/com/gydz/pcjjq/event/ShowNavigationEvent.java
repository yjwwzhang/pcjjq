package com.gydz.pcjjq.event;

/**
 * 创建时间:2018/7/2 17:36
 */
public class ShowNavigationEvent {

    private boolean isClickNavigation;

    public ShowNavigationEvent(boolean isClickNavigation) {
        this.isClickNavigation = isClickNavigation;
    }

    public boolean isClickNavigation() {
        return isClickNavigation;
    }

    public void setClickNavigation(boolean clickNavigation) {
        isClickNavigation = clickNavigation;
    }

}
