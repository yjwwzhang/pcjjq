package com.gydz.pcjjq.event;

/**
 * 创建时间:2018/6/26 16:35
 */
public class CmdT4482Event {

    private int totalNumber;
    private int oneIsVacancy;
    private int twoIsVacancy;
    private int threeIsVacancy;
    private int fourIsVacancy;
    private int oneIsPause;
    private int twoIsPause;
    private int threeIsPause;
    private int fourIsPause;
    private int passengerNumber;
    private int isGettingOff;
    private int isPrint;

    @Override
    public String toString() {
        return "CmdT4482Event{" +
                "totalNumber=" + totalNumber +
                ", oneIsVacancy=" + oneIsVacancy +
                ", twoIsVacancy=" + twoIsVacancy +
                ", threeIsVacancy=" + threeIsVacancy +
                ", fourIsVacancy=" + fourIsVacancy +
                ", oneIsPause=" + oneIsPause +
                ", twoIsPause=" + twoIsPause +
                ", threeIsPause=" + threeIsPause +
                ", fourIsPause=" + fourIsPause +
                ", passengerNumber=" + passengerNumber +
                ", isGettingOff=" + isGettingOff +
                ", isPrint=" + isPrint +
                '}';
    }

    public CmdT4482Event(int totalNumber, int oneIsVacancy, int twoIsVacancy, int threeIsVacancy, int fourIsVacancy, int oneIsPause, int twoIsPause, int threeIsPause, int fourIsPause, int passengerNumber, int isGettingOff, int isPrint) {
        this.totalNumber = totalNumber;
        this.oneIsVacancy = oneIsVacancy;
        this.twoIsVacancy = twoIsVacancy;
        this.threeIsVacancy = threeIsVacancy;
        this.fourIsVacancy = fourIsVacancy;
        this.oneIsPause = oneIsPause;
        this.twoIsPause = twoIsPause;
        this.threeIsPause = threeIsPause;
        this.fourIsPause = fourIsPause;
        this.passengerNumber = passengerNumber;
        this.isGettingOff = isGettingOff;
        this.isPrint = isPrint;
    }

    public int getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(int totalNumber) {
        this.totalNumber = totalNumber;
    }

    public int getOneIsVacancy() {
        return oneIsVacancy;
    }

    public void setOneIsVacancy(int oneIsVacancy) {
        this.oneIsVacancy = oneIsVacancy;
    }

    public int getTwoIsVacancy() {
        return twoIsVacancy;
    }

    public void setTwoIsVacancy(int twoIsVacancy) {
        this.twoIsVacancy = twoIsVacancy;
    }

    public int getThreeIsVacancy() {
        return threeIsVacancy;
    }

    public void setThreeIsVacancy(int threeIsVacancy) {
        this.threeIsVacancy = threeIsVacancy;
    }

    public int getFourIsVacancy() {
        return fourIsVacancy;
    }

    public void setFourIsVacancy(int fourIsVacancy) {
        this.fourIsVacancy = fourIsVacancy;
    }

    public int getOneIsPause() {
        return oneIsPause;
    }

    public void setOneIsPause(int oneIsPause) {
        this.oneIsPause = oneIsPause;
    }

    public int getTwoIsPause() {
        return twoIsPause;
    }

    public void setTwoIsPause(int twoIsPause) {
        this.twoIsPause = twoIsPause;
    }

    public int getThreeIsPause() {
        return threeIsPause;
    }

    public void setThreeIsPause(int threeIsPause) {
        this.threeIsPause = threeIsPause;
    }

    public int getFourIsPause() {
        return fourIsPause;
    }

    public void setFourIsPause(int fourIsPause) {
        this.fourIsPause = fourIsPause;
    }

    public int getPassengerNumber() {
        return passengerNumber;
    }

    public void setPassengerNumber(int passengerNumber) {
        this.passengerNumber = passengerNumber;
    }

    public int getIsGettingOff() {
        return isGettingOff;
    }

    public void setIsGettingOff(int isGettingOff) {
        this.isGettingOff = isGettingOff;
    }

    public int getIsPrint() {
        return isPrint;
    }

    public void setIsPrint(int isPrint) {
        this.isPrint = isPrint;
    }
}
