package com.gydz.pcjjq.event;

import com.gydz.pcjjq.model.bean.Cmd4403Bean;

/**
 * 创建时间:2018/6/28 14:42
 */
public class CmdR4403Event {

    private Cmd4403Bean mCmd4403Bean;

    @Override
    public String toString() {
        return "CmdR4403Event{" +
                "mCmd4403Bean=" + mCmd4403Bean +
                '}';
    }

    public CmdR4403Event(Cmd4403Bean cmd4403Bean) {
        mCmd4403Bean = cmd4403Bean;
    }

    public Cmd4403Bean getCmd4403Bean() {
        return mCmd4403Bean;
    }

    public void setCmd4403Bean(Cmd4403Bean cmd4403Bean) {
        mCmd4403Bean = cmd4403Bean;
    }
}
