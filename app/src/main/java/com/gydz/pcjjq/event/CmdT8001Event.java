package com.gydz.pcjjq.event;

/**
 * 创建时间:2018/6/27 11:25
 */
public class CmdT8001Event {

    private String cmd;
    private int reply;

    @Override
    public String toString() {
        return "CmdT8001Event{" +
                "cmd=" + cmd +
                ", reply=" + reply +
                '}';
    }

    public CmdT8001Event(String cmd, int reply) {
        this.cmd = cmd;
        this.reply = reply;
    }

    public String getCmd() {
        return cmd;
    }

    public void setCmd(String cmd) {
        this.cmd = cmd;
    }

    public int getReply() {
        return reply;
    }

    public void setReply(int reply) {
        this.reply = reply;
    }
}
