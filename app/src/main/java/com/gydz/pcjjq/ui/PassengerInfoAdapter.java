package com.gydz.pcjjq.ui;

import android.annotation.SuppressLint;

import com.chad.library.adapter.base.BaseMultiItemQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.gydz.pcjjq.R;
import com.gydz.pcjjq.app.Constants;
import com.gydz.pcjjq.model.bean.PassengerInfoBean;
import com.gydz.pcjjq.utils.ConvertUtilsPlus;

import java.util.List;

/**
 * 创建时间:2018/6/25 16:14
 */
public class PassengerInfoAdapter extends BaseMultiItemQuickAdapter<PassengerInfoBean, BaseViewHolder> {

    public PassengerInfoAdapter(List<PassengerInfoBean> data) {
        super(data);
        addItemType(Constants.ITEM_PASSENGER_INFO_1, R.layout.item_passenger_info_1);
        addItemType(Constants.ITEM_PASSENGER_INFO_2, R.layout.item_passenger_info_2);
    }

    @SuppressLint("DefaultLocale")
    @Override
    protected void convert(BaseViewHolder helper, PassengerInfoBean item) {
        switch (helper.getItemViewType()) {
            case Constants.ITEM_PASSENGER_INFO_1:
                helper.setText(R.id.tv_pessenger_number, String.format("乘客%d", item.getPassengerNumber()));
                helper.setText(R.id.tv_should_pay, String.format("%.2f元", item.getShouldPay() / 100f));
                helper.setText(R.id.tv_real_pay, String.format("%.2f元", item.getRealPay() / 100f));
                helper.setText(R.id.tv_mileage, String.format("公里：%.2f", item.getMileage() * 1f));
                helper.setText(R.id.tv_wait_time, String.format("等候：%s", ConvertUtilsPlus.getStringTime(item.getWaitTime())));
                if (item.getPauseStatus() == 1) {
                    helper.setText(R.id.btn_pause, "计费");
                } else {
                    helper.setText(R.id.btn_pause, "暂停");
                }
                helper.addOnClickListener(R.id.btn_navigation)
                        .addOnClickListener(R.id.btn_get_off)
                        .addOnClickListener(R.id.btn_pause);
                break;
            case Constants.ITEM_PASSENGER_INFO_2:
                helper.setText(R.id.tv_mileage, String.format("公里：%.2f", item.getMileage() * 1f));
                helper.setText(R.id.tv_wait_time, String.format("等候：%s", ConvertUtilsPlus.getStringTime(item.getWaitTime())));
                helper.setText(R.id.tv_invoice, String.format("%.2f元", item.getRealPay() / 100f));
                helper.setText(R.id.tv_seat_number, item.getPassengerNumber() + "");
                helper.addOnClickListener(R.id.btn_get_on)
                        .addOnClickListener(R.id.btn_invoice);
                break;
        }
    }
}
