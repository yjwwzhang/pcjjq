package com.gydz.pcjjq.ui;

import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.baidu.navisdk.adapter.BNRoutePlanNode;
import com.baidu.navisdk.adapter.BaiduNaviManagerFactory;
import com.baidu.navisdk.adapter.IBNRoutePlanManager;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.gydz.pcjjq.R;
import com.gydz.pcjjq.app.Constants;
import com.gydz.pcjjq.base.SimpleFragment;
import com.gydz.pcjjq.event.CmdT4482Event;
import com.gydz.pcjjq.model.bean.Cmd4403Bean;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

/**
 * 创建时间:2018/7/2 15:09
 */
public class PassengerInfoFragment extends SimpleFragment {

    @BindView(R.id.rcy_passenger_info)
    RecyclerView mRcyPassengerInfo;
    private Cmd4403Bean mCmd4403Bean  = new Cmd4403Bean();
    private PassengerInfoAdapter mPassengerInfoAdapter;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_passenger_info;
    }

    @Override
    protected void initEventAndData() {
        mRcyPassengerInfo.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        mPassengerInfoAdapter = new PassengerInfoAdapter(mCmd4403Bean.getPassengerInfos());
        mPassengerInfoAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.btn_navigation:
                        ToastUtils.showLong("导航");
//                        EventBus.getDefault().post(new ShowNavigationEvent(true));
                        List<BNRoutePlanNode> list = new ArrayList<BNRoutePlanNode>();
                        list.add(new BNRoutePlanNode(116.30784537597782, 40.057009624099436, "百度大厦", "百度大厦", BNRoutePlanNode.CoordinateType.BD09LL));
                        list.add(new BNRoutePlanNode(116.40386525193937, 39.915160800132085, "北京天安门", "北京天安门", BNRoutePlanNode.CoordinateType.BD09LL));
                        BaiduNaviManagerFactory.getRoutePlanManager().routeplanToNavi(
                                list,
                                IBNRoutePlanManager.RoutePlanPreference.ROUTE_PLAN_PREFERENCE_DEFAULT,
                                null,
                                new Handler(Looper.getMainLooper()) {
                                    @Override
                                    public void handleMessage(Message msg) {
                                        switch (msg.what) {
                                            case IBNRoutePlanManager.MSG_NAVI_ROUTE_PLAN_START:
                                                LogUtils.e("算路开始");
                                                break;
                                            case IBNRoutePlanManager.MSG_NAVI_ROUTE_PLAN_SUCCESS:
                                                LogUtils.e("算路成功");
                                                break;
                                            case IBNRoutePlanManager.MSG_NAVI_ROUTE_PLAN_FAILED:
                                                LogUtils.e("算路失败");
                                                break;
                                            case IBNRoutePlanManager.MSG_NAVI_ROUTE_PLAN_TO_NAVI:

                                                LogUtils.e("算路成功准备进入导航");
                                                Intent intent = new Intent(getActivity(),
                                                        NaviGuideActivity.class);
                                                startActivity(intent);
                                                break;
                                            default:
                                                // nothing
                                                break;
                                        }
                                    }
                                });

                        break;
                    case R.id.btn_get_off:
                        ToastUtils.showLong("下车");
                        mCmd4403Bean.getPassengerInfos().get(position).setSeatStatus(Constants.ITEM_PASSENGER_INFO_2);
                        EventBus.getDefault().post(new CmdT4482Event(mCmd4403Bean.getPassengerTotal() - 1, mCmd4403Bean.getPassengerInfos().get(0).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(1).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(2).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(3).getSeatStatus(),
                                mCmd4403Bean.getPassengerInfos().get(0).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(1).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(2).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(3).getPauseStatus(), position + 1, 0, 0));
                        break;
                    case R.id.btn_pause:
                        if (mCmd4403Bean.getPassengerInfos().get(position).getPauseStatus() != 1) {
                            ToastUtils.showLong("已暂停");
                            mCmd4403Bean.getPassengerInfos().get(position).setPauseStatus(1);
                            adapter.notifyItemChanged(position);
                            EventBus.getDefault().post(new CmdT4482Event(mCmd4403Bean.getPassengerTotal(), mCmd4403Bean.getPassengerInfos().get(0).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(1).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(2).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(3).getSeatStatus(),
                                    mCmd4403Bean.getPassengerInfos().get(0).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(1).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(2).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(3).getPauseStatus(), position + 1, 1, 0));
                        } else {
                            ToastUtils.showLong("行驶中");
                            mCmd4403Bean.getPassengerInfos().get(position).setPauseStatus(0);
                            adapter.notifyItemChanged(position);
                            EventBus.getDefault().post(new CmdT4482Event(mCmd4403Bean.getPassengerTotal(), mCmd4403Bean.getPassengerInfos().get(0).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(1).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(2).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(3).getSeatStatus(),
                                    mCmd4403Bean.getPassengerInfos().get(0).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(1).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(2).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(3).getPauseStatus(), position + 1, 1, 0));
                        }
                        break;
                    case R.id.btn_get_on:
                        ToastUtils.showLong("上车");
                        mCmd4403Bean.getPassengerInfos().get(position).setSeatStatus(Constants.ITEM_PASSENGER_INFO_1);
                        EventBus.getDefault().post(new CmdT4482Event(mCmd4403Bean.getPassengerTotal() + 1, mCmd4403Bean.getPassengerInfos().get(0).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(1).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(2).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(3).getSeatStatus(),
                                mCmd4403Bean.getPassengerInfos().get(0).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(1).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(2).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(3).getPauseStatus(), position + 1, 1, 0));
                        break;
                    case R.id.btn_invoice:
                        ToastUtils.showLong("打印发票");
                        EventBus.getDefault().post(new CmdT4482Event(mCmd4403Bean.getPassengerTotal(), mCmd4403Bean.getPassengerInfos().get(0).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(1).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(2).getSeatStatus(), mCmd4403Bean.getPassengerInfos().get(3).getSeatStatus(),
                                mCmd4403Bean.getPassengerInfos().get(0).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(1).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(2).getPauseStatus(), mCmd4403Bean.getPassengerInfos().get(3).getPauseStatus(), position + 1, 0, 1));
                        break;
                }
            }
        });
        mRcyPassengerInfo.setAdapter(mPassengerInfoAdapter);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessagePassengerInfoBeanEvent(Cmd4403Bean cmd4403Bean) {
        mCmd4403Bean = cmd4403Bean;
        mPassengerInfoAdapter.setNewData(cmd4403Bean.getPassengerInfos());
    }

}
