package com.gydz.pcjjq.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gydz.pcjjq.R;
import com.gydz.pcjjq.event.ShowPassengerInfoEvent;

import org.greenrobot.eventbus.EventBus;

import me.yokeyword.fragmentation.SupportFragment;

/**
 * 创建时间:2018/7/2 15:28
 */
public class NavigationFragment extends SupportFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_navigation, container, false);
        return view;
    }


    @Override
    public boolean onBackPressedSupport() {
        EventBus.getDefault().post(new ShowPassengerInfoEvent(false));
        return true;
    }

}
